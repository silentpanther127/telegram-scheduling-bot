import telebot
import configparser
import datetime
import getopt
import os
import signal
import time  # for time.sleep()
import sys  # for sys.exit()
import logging

usage = """
NAME
            schedule - schedule an event within the next days
            
SYNOPSIS
            /schedule [--skip NUMBER] [--options OPTIONS] days question...
            
DESCRIPTION
            schedule creates a poll for everyone to vote when they are available.
            The first positional argument is the number of days starting from today.
            The rest is a question to ask. 
                
                /schedule 7 Next round?
                Will produce a poll to schedule 'Next round?' within 7 days, starting today.
                
                /schedule --skip 2 --options afternoon,evening 14 get together at Lou
                Will produce a poll to schedule the 'get together at Luis'' within two weeks,
                starting from the day after tomorrow, with the options afternoon and evening
                for every day.

OPTIONS
            -s NUMBER
            --skip NUMBER
                    Skip the number of days for scheduling, default is to start from today.
            
            -o OPTIONS
            --options OPTIONS
                    Per day, offer OPTIONS. Where OPTIONS is a comma separated list of options.
                    e.g. forenoon,afternoon,evening so that each day is split into these options.
"""


usage_hint = "\nUse /usage for a full description."


day_lookup_list = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

log = telebot.logger
telebot.logger.setLevel(logging.DEBUG)

# read token from config file, store it in variable token
config = configparser.RawConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '../config.cfg'))
config_dict = dict(config.items('telegram'))
token = config_dict['token']

bot = telebot.TeleBot(token, parse_mode=None)


@bot.message_handler(commands=['start', 'help', 'usage'])
def send_welcome(message):
    log.info("received message: " + str(message))
    bot.send_message(message.chat.id, usage)


@bot.message_handler(commands=['schedule'])
def create_poll(message):

    log.info("received message: " + str(message))

    try:
        opts, args = getopt.getopt(message.text.split(" ")[1:], "s:o:", ["skip=", "options="])
    except getopt.GetoptError as err:
        bot.reply_to(message, err)
        return

    skip_days = 0
    options_per_day = []
    for o, a in opts:
        if o in ("-s", "--skip"):
            try:
                skip_days = int(a)
            except ValueError as err:
                bot.reply_to(message,
                             "Use an Integer to specify number of days to skip." + usage_hint)
                return
            if skip_days < 0:
                bot.reply_to(message,
                             "Less than zero days as to skip are not possible!" + usage_hint)
                return
        elif o in ("-o", "--options"):
            options_per_day = a.split(",")

    if len(args) < 2:
        bot.reply_to(message,
                     "Specify the number of days that should become voting options as positional argument Integer."
                     + "\n" +
                     "Followed by a question to ask or title for the poll." + usage_hint)
        return
    try:
        parsed_days = int(args[0])
    except ValueError as err:
        bot.reply_to(message,
                     "Positional argument specifying number of days is not an Integer: " + args[0] + usage_hint)
        return
    if parsed_days <= 0:
        bot.reply_to(message,
                     "Less than or zero days as options are not possible!" + usage_hint)
        return

    if len(options_per_day) < 2 and parsed_days < 2:
        bot.reply_to(message,
                     "At least two days or options per day are required!" + usage_hint)
        return

    options = []
    for i in range(skip_days, parsed_days + skip_days):
        dt = datetime.datetime.today() + datetime.timedelta(days=i)
        formatted_day = day_lookup_list[dt.weekday()] + " " + dt.strftime("%d.%m.")
        if len(options_per_day) is 0:
            options.append(formatted_day)
        else:
            for o in options_per_day:
                options.append(formatted_day + " " + o)

    question = " ".join(args[1:])

    if len(options) > 10:
        bot.reply_to(message,
                     "Telegram does not allow polls with more than 10 options.")
        return

    bot.send_poll(
        chat_id=message.chat.id,
        question=question,
        options=options,
        is_anonymous=False,
        allows_multiple_answers=True
    )


def main():
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    while True:
        try:
            log.debug("starting...")
            bot.polling()
            print("done polling")
        except Exception as err:
            log.warn("Bot polling error: {0}".format(err.args))
            bot.stop_polling()
            time.sleep(30)
            log.debug("trying to restart...")


def signal_handler(signal_number, frame):
    log.info('Received signal ' + str(signal_number) + '. Exiting...')
    bot.stop_polling()
    sys.exit(0)


if __name__ == "__main__":
    main()
