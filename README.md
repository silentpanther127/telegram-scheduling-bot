# A Telegram Bot for Scheduling

This is a Telegram Bot that uses Polls with multiple votes allowed to Schedule something in a chat Group.
It is there to easily generate polls for scheduling, by creating a poll with the next X Days available as options.
This should eradicate the tedious work of typing consecutive dates in by hand.

Try it at [t.me/dnd_scheduling_bot](t.me/dnd_scheduling_bot).


```
NAME
            schedule - schedule an event within the next days
            
SYNOPSIS
            /schedule [--skip NUMBER] [--options OPTIONS] days question...
            
DESCRIPTION
            schedule creates a poll for everyone to vote when they are available.
            The first positional argument is the number of days starting from today.
            The rest is a question to ask. 
                
                /schedule 7 Next round?
                Will produce a poll to schedule 'Next round?' within 7 days, starting today.
                
                /schedule --skip 2 --options afternoon,evening 14 get together at Lou
                Will produce a poll to schedule the 'get together at Luis'' within two weeks,
                starting from the day after tomorrow, with the options afternoon and evening
                for every day.

OPTIONS
            -s NUMBER
            --skip NUMBER
                    Skip the number of days for scheduling, default is to start from today.
            
            -o OPTIONS
            --options OPTIONS
                    Per day, offer OPTIONS. Where OPTIONS is a comma separated list of options.
                    e.g. forenoon,afternoon,evening so that each day is split into these options.
```